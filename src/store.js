import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
export default new Vuex.Store({
  state: {
    user: {
      loggedIn: false,
      data: null
    }
  },
  getters: {
    user(state){
      return state.user
    }
  },
  mutations: {
    SET_LOGGED_IN(state, value) {
      state.user.loggedIn = value;
    },
    SET_USER(state, data) {
      state.user.data = data;
    },
    UPDATE_PROFILE(state,data){
      for(let keyAtt in data){
        if(Object.prototype.hasOwnProperty.call(data, keyAtt)){
          state.user.data[keyAtt]=data[keyAtt]
        }else{ state.user.data[keyAtt]=data[keyAtt]}
      }
      
    }
  },
  actions: {
    fetchUser({ commit }, user) {
      commit("SET_LOGGED_IN", user !== null);
      if (user) {
        commit("SET_USER", {
          displayName: user.displayName,
          email: user.email
        });
      } else {
        commit("SET_USER", null);
      }
    },
    updateUser({ commit },user){
      commit('UPDATE_PROFILE',user)
    }
  }
});