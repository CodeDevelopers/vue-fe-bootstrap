import Vue from "vue";
import App from "./App.vue";
import router from "./routes/index";
//import * as firebase from "firebase";
import firebase from 'firebase/app';
 import 'firebase/auth';
 import 'firebase/database';
 import 'firebase/firestore';
 import 'firebase/storage';

import store from "./store";
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Make BootstrapVue available throughout your project
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.config.productionTip = false;

const configOptions = {
    apiKey: "AIzaSyDVUvWtoL5QpTQcul4_E50omMUhuDukUHc",
    authDomain: "login-auth-f4813.firebaseapp.com",
    databaseURL: "https://login-auth-f4813.firebaseio.com",
    projectId: "login-auth-f4813",
    storageBucket: "",
    messagingSenderId: "1028632771677",
    appId: "1:1028632771677:web:1ce98b0ddf17da21"
};

firebase.initializeApp(configOptions);

firebase.auth().onAuthStateChanged(user => {
  store.dispatch("fetchUser", user);
});

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");