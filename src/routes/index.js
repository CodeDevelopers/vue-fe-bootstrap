import Vue from 'vue';
import Router from 'vue-router';
import Login from "../components/Login"
import Register from '../components/Register'
import Dashboard from '../components/Dashboard'
import Profile from '../components/Profile';
import Welcome from '../components/Welcome'
Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        children: [
            {
                path: '/profile',
                name: 'Profile',
                component: Profile
            }, {
                path: '',
                name: 'Welcome',
                component: Welcome
            }]
    }
]
});

export default router